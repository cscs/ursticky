/**
 * Returns a promise that resolves when the page DOM has been loaded.
 * @returns {Promise<void>}
 */
export function waitForLoad() {
  return new Promise((resolve) => {
    // If already loaded, fire immediately
    if (/complete|interactive|loaded/.test(document.readyState)) {
      resolve();
    } else {
      document.addEventListener("DOMContentLoaded", resolve);
    }
  });
}

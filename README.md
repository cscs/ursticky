# ursticky

A storage-less pastebin or sticky note tool.

By compressing the text contents and adding them to the end hash of the URL nothing need to be sent to the server and the pastebin host can be a static webpage.

The compression uses [brotli-wasm](https://github.com/httptoolkit/brotli-wasm) in the browser. The compressed data is then encoded with Base66 to be URL safe (percent encoding would just grow the length).

The maximum paste size is dependent on the URL before the hash. URLs have a 2048 character length limit, so the paste is limited to only generate URLs that fit in that limit.

<small> _Originally forked from [Tahnaroskakori](https://gitlab.com/Nicd/t) by @Nicd_ </small>

## Working Model  
**[cscs.gitlab.io/ursticky](https://cscs.gitlab.io/ursticky/)**


<br></br>

### Donate  

Everything is free, but you can donate using these:  

<a href='https://ko-fi.com/X8X0VXZU' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a> &nbsp; <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52'><img height='36' style='border:0px;height:36px;' src='https://gitlab.com/cscs/resources/raw/master/paypalkofi.png' border='0' alt='Donate with Paypal' />  

export class ViewMode {
  static VIEW = Symbol("view");
  static EDIT = Symbol("edit");
}

Object.freeze(ViewMode);
